using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    private bool finnished = false;
    public float ZavrsnoVrijeme;
    public int Minutes;
    public int BrCoins;
    public int zvjezdice;
    public int NoviciciSuma;
    private string TrenutnaScena;
    private string TrenutnaScenaZvjezice;
    private string zavrsetak;
    public int zsuma;
    [SerializeField]

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;  //Time.time kad aplikacija pocne, start kad je moj objekt na mapi
        Scene scene = SceneManager.GetActiveScene();   // Vracamo trenutnu aktivnu scenu kako bidobili naziv trenutne scene
        TrenutnaScena = scene.name;
        TrenutnaScenaZvjezice = TrenutnaScena + "Zvijezdice";

    }

    // Update is called once per frame
    void Update()
    {
        if (finnished)
        {
            return;

        }


        float t = Time.time - startTime;  //zelimo postavit timer malo poslije da krene napravit cemo restore startTime
        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("f0");  //f0 bez decimala, f2 na dvije

        timerText.text = "Time: " + minutes + ":" + seconds;
    }

    public void Finnish()
    {
        finnished = true;
        timerText.color = Color.yellow;
        Debug.Log("Zavrsno vrijeme" + ZavrsnoVrijeme);
        ZavrsnoVrijeme = Time.time - startTime;
        Minutes = ((int)ZavrsnoVrijeme / 60);
        Debug.Log("Zavrsna minuta" + Minutes);
        string sekunda = (ZavrsnoVrijeme % 60).ToString("f0");
        Debug.Log("Zavrsna sekunda" + sekunda);


        if (TrenutnaScena == "Level7" || TrenutnaScena == "Level8")
        {
            if (Minutes < 3)
            {
                PlayerManager.finnished_3zvijezdice = true;
                BrCoins = 300;
                zvjezdice = 3;
                Debug.Log("dobio si za odlicno vrijeme coins" + BrCoins);
            }

            if (Minutes <= 4 && Minutes >= 3)
            {
                PlayerManager.finnished_2zvijezdice = true;
                zvjezdice = 0;
                BrCoins = 200;
            }
            if (Minutes > 4 && Minutes < 5)
            {
                PlayerManager.finnished_1zvijezdice = true;
                zvjezdice = 0;
                BrCoins = 100;
            }

            if (Minutes >= 6)
            {
                PlayerManager.finnished_0zvijezdice = true;
                zvjezdice = 0;
                BrCoins = 0;
            }

        }
        else
        {
            if (Minutes < 6)
            {
                PlayerManager.finnished_3zvijezdice = true;
                BrCoins = 300;
                zvjezdice = 3;
                Debug.Log("dobio si za odlicno vrijeme coins" + BrCoins);
            }

            if (Minutes <= 8 && Minutes >= 6)
            {
                PlayerManager.finnished_2zvijezdice = true;
                zvjezdice = 0;
                BrCoins = 200;
            }
            if (Minutes > 8 && Minutes < 10)
            {
                PlayerManager.finnished_1zvijezdice = true;
                zvjezdice = 0;
                BrCoins = 100;
            }

            if (Minutes >= 10)
            {
                PlayerManager.finnished_0zvijezdice = true;
                zvjezdice = 0;
                BrCoins = 0;
            }
        }
    }
    void OnTriggerEnter(Collider target)
    {
        if (target.CompareTag("Goal"))
        {

            Debug.Log("Spremanje novcica");
            NoviciciSuma = PlayerPrefs.GetInt("TNCoin", 0); // gledamo ako postoji getint i koju ima vrijednost ako nema nov�i�a onda �e vratiti 0  
            NoviciciSuma += BrCoins;
            PlayerPrefs.SetInt("TNCoin", NoviciciSuma);  //zapisujemo novi br novcica u prefs
            PlayerPrefs.SetInt(TrenutnaScenaZvjezice, zvjezdice);
            zsuma= PlayerPrefs.GetInt("ZvijezdiceSuma", 0);
            zsuma += zvjezdice;
            PlayerPrefs.SetInt("ZvijezdiceSuma", zsuma);
            Debug.Log("brcoins" + BrCoins);
            Debug.Log("novcicisuma" + NoviciciSuma);


        }
    }

   
}
