using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShopManager : MonoBehaviour
{
    public int currentDroneIndex = 0;
    public GameObject[] DroneModels;

    public DroneBlueprint[] drones;
    public Button buyButton;
    public int CoinsSuma = 0;
    
    public int currentCoins_br = 0;
    public int level2;
    public int level4;
    public int level8;
    public int level6;
    public int dron1;
    public int dron2;
    public int dron3;
    public int dron4;
    public int z1;
    public int z2;
    public int z3;
    public int z4;
    public int z5;
    public int z6;
    public int z7;
    public int z8;
    public int z9;
    public int z10;
    public int zsuma;
    public int zsuma2;
    [SerializeField]
    private Text currentCoins;
    public Text sumZvijezdica;
   
    // Start is called before the first frame update
    void Start()
    {
        zsuma = PlayerPrefs.GetInt("ZvijezdiceSuma", 0);
        
        if (PlayerPrefs.HasKey("TNCoin"))
        {
            currentCoins_br = PlayerPrefs.GetInt("TNCoin");
            CoinsSuma = currentCoins_br;
        }
        else
        {
            CoinsSuma = 0;
        }

        foreach (DroneBlueprint drone in drones)
        {
            if (drone.price == 0)
                drone.isUnlocked = true;
            else
                drone.isUnlocked = PlayerPrefs.GetInt(drone.name, 0) == 0 ? false : true;
        }


        currentDroneIndex = PlayerPrefs.GetInt("SelectedDrone", 0);
        foreach (GameObject drone in DroneModels)
            drone.SetActive(false);

        DroneModels[currentDroneIndex].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Otkljucavanje();
        currentCoins.text = "Coins: " + CoinsSuma.ToString();
        sumZvijezdica.text = "Zvijezdica:" + zsuma.ToString();
    }

    public void ChangeNext()
    {
        DroneModels[currentDroneIndex].SetActive(false);

        currentDroneIndex++;
        if (currentDroneIndex == DroneModels.Length)
            currentDroneIndex = 0;

        DroneModels[currentDroneIndex].SetActive(true);
        // PlayerPrefs.SetInt("SelectedDrone", currentDroneIndex);
        DroneBlueprint c = drones[currentDroneIndex];
        if (!c.isUnlocked) return;
    }

    public void ChangePrevious()
    {
        DroneModels[currentDroneIndex].SetActive(false);

        currentDroneIndex--;
        if (currentDroneIndex == -1)
            currentDroneIndex = DroneModels.Length - 1;

        DroneModels[currentDroneIndex].SetActive(true);

        DroneBlueprint c = drones[currentDroneIndex];
        if (!c.isUnlocked) return;

        PlayerPrefs.SetInt("SelectedDrone", currentDroneIndex);
    }
    public void UnlockDrone()
    {
        Debug.Log("Stisnuo si gumbic");
        DroneBlueprint c = drones[currentDroneIndex];
        PlayerPrefs.SetInt(c.name, 1);
        PlayerPrefs.SetInt("SelectedDrone", currentDroneIndex);
        c.isUnlocked = true;
        PlayerPrefs.SetInt("TNCoin", PlayerPrefs.GetInt("TNCoin", 0) - c.price);
        PlayerPrefs.SetInt("ZvijezdiceSuma", PlayerPrefs.GetInt("ZvijezdiceSuma", 0) - c.zvjezdice);
        currentCoins_br = PlayerPrefs.GetInt("TNCoin");
        zsuma2= PlayerPrefs.GetInt("ZvijezdiceSuma");
        CoinsSuma = currentCoins_br;
        zsuma = zsuma2;



    }
    private void Otkljucavanje()
    {
        z1 = PlayerPrefs.GetInt("Level1Zvijezdice", 0);
        z2 = PlayerPrefs.GetInt("Level2Zvijezdice", 0);
        z3 = PlayerPrefs.GetInt("Level3Zvijezdice", 0);
        z4 = PlayerPrefs.GetInt("Level4Zvijezdice", 0);
        z5 = PlayerPrefs.GetInt("Level5Zvijezdice", 0);
        z6 = PlayerPrefs.GetInt("Level6Zvijezdice", 0);
        z7 = PlayerPrefs.GetInt("Level7Zvijezdice", 0);
        z8 = PlayerPrefs.GetInt("Level8Zvijezdice", 0);
        z9 = PlayerPrefs.GetInt("Level9Zvijezdice", 0);
        z10 = PlayerPrefs.GetInt("Level10Zvijezdice", 0);
        zsuma = PlayerPrefs.GetInt("ZvijezdiceSuma", 0);
       
        level4 = PlayerPrefs.GetInt("Level4", 0);
        level6 = PlayerPrefs.GetInt("Level6", 0);
        level8 = PlayerPrefs.GetInt("Level8", 0);
        dron1 = PlayerPrefs.GetInt("_Drone [Toad] - Warhawk", 0);
        dron2 = PlayerPrefs.GetInt("_Drone [BumbleBee] - Electric", 0);
        dron3= PlayerPrefs.GetInt("_Drone [Quad] - Police", 0);
        dron4 = PlayerPrefs.GetInt("_Drone [UFO] - Alien", 0);

        DroneBlueprint c = drones[currentDroneIndex];
        buyButton.gameObject.SetActive(false);
        if (c.isUnlocked)   
        {
            buyButton.gameObject.SetActive(false);
        }
     
            if (dron1 == 1 && !c.isUnlocked )
            {
                if (c.index == 1 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma",0))
                {
                    Debug.Log("idem u 1");
                    buyButton.gameObject.SetActive(true);
                    buyButton.GetComponentInChildren<Text>().text = "Common: " + c.zvjezdice +" stars";
                }
               
                if (c.index == 2 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
                {
                    Debug.Log("idem u 2");
                    buyButton.gameObject.SetActive(true);
                    buyButton.GetComponentInChildren<Text>().text = "Rare: " + c.zvjezdice + " stars"; 
            }
               
                if (c.index == 3 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
                {
                    Debug.Log("idem u 3");
                    buyButton.gameObject.SetActive(true);
                    buyButton.GetComponentInChildren<Text>().text = "Legendary: " + c.zvjezdice + " stars"; 
            }
                
            }

        if (dron2 == 1 && !c.isUnlocked)
        {
            if (c.index == 5 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
            {
                Debug.Log("idem u 1");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Common: " + c.zvjezdice + " stars";
            }

            if (c.index == 6 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
            {
                Debug.Log("idem u 2");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Rare: " + c.zvjezdice + " stars"; 
            }

            if (c.index == 7 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
            {
                Debug.Log("idem u 3");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Legendary: " + c.zvjezdice + " stars"; 
            }

        }

        if (dron3 == 1 && !c.isUnlocked)
        {
            if (c.index == 9 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
            {
                Debug.Log("idem u 1");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Common: " + c.zvjezdice + " stars";
            }

            if (c.index == 10 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
            {
                Debug.Log("idem u 2");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Rare: " + c.zvjezdice + " stars"; 
            }

            if (c.index == 11 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && c.zvjezdice < PlayerPrefs.GetInt("ZvijezdiceSuma", 0))
            {
                Debug.Log("idem u 3");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Legendary: " + c.zvjezdice + " stars"; 
            }

        }
        if (dron4 == 1 && !c.isUnlocked)
        {
            if (c.index == 13 && c.price < PlayerPrefs.GetInt("TNCoin", 0))
            {
                Debug.Log("idem u 1");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Common: " + c.zvjezdice + " stars";
            }

            if (c.index == 14 && c.price < PlayerPrefs.GetInt("TNCoin", 0))
            {
                Debug.Log("idem u 2");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Rare: " + c.zvjezdice + " stars"; 
            }

            if (c.index == 15 && c.price < PlayerPrefs.GetInt("TNCoin", 0))
            {
                Debug.Log("idem u 3");
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Legendary: " + c.zvjezdice + " stars";
            }

        }

        if (level4 == 1 && c.index == 4 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && !c.isUnlocked)
            {
                Debug.Log("lev4" + level4);
                Debug.Log("ind" + c.index);
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Buy-" + c.price;
               
            }
           

            if (level6 == 1 && c.index == 8 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && !c.isUnlocked)
            {
                Debug.Log("lev6" + level8);
                Debug.Log("ind2" + c.index);
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Buy-" + c.price;
               
            }
          
            if (level8 == 1 && c.index == 12 && c.price < PlayerPrefs.GetInt("TNCoin", 0) && !c.isUnlocked)
            {
                Debug.Log("lev8" + level8);
                Debug.Log("ind3" + c.index);
                buyButton.gameObject.SetActive(true);
                buyButton.GetComponentInChildren<Text>().text = "Buy-" + c.price;
              
            }
        }
    }

