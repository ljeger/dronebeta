using UnityEngine.SceneManagement;
using UnityEngine;


public class Events : MonoBehaviour
{
    public string TrenutnaScena;
    public void ReplayGame()
    {
        SceneManager.LoadScene(TrenutnaScena);


    }

    public void QuitGame()
    {
        SceneManager.LoadScene("Menu");
        
    }
    void Start()
    {
        {
            Scene scene = SceneManager.GetActiveScene();   // Vracamo trenutnu aktivnu scenu kako bidobili naziv trenutne scene
            TrenutnaScena = scene.name;
        }

    }
}