using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectManager : MonoBehaviour
{
    public int currentDroneIndex_;
    public GameObject[] DroneModels_;
  public DroneBluePrintGaraza[] drones_;
    public GameObject PlayBTN;
    public GameObject zakljucano;
    //[SerializeField]
    void Start()
    {
        foreach (DroneBluePrintGaraza drone in drones_)
        {
           //cita iz playerprefsa ako je dron otkljucan
                drone.isUnlocked = PlayerPrefs.GetInt(drone.name, 0) == 0 ? false : true;
        }


        currentDroneIndex_ = PlayerPrefs.GetInt("SelectedDrone", 0);
        foreach (GameObject drone in DroneModels_)
            drone.SetActive(false);
            zakljucano.SetActive(true);
        DroneModels_[currentDroneIndex_].SetActive(true);
    }




       
    void Update()
    {
        
    }

    public void ChangeNext()
    {
        DroneModels_[currentDroneIndex_].SetActive(false);

        currentDroneIndex_++;
        if (currentDroneIndex_ == DroneModels_.Length)
            currentDroneIndex_ = 0;

       
        DroneBluePrintGaraza c = drones_[currentDroneIndex_];
        DroneModels_[currentDroneIndex_].SetActive(true);
        PlayBTN.gameObject.SetActive(false);
        zakljucano.SetActive(true);
        //ako je otkljucan stavi ga na true
        if (c.isUnlocked)
        {
            PlayBTN.gameObject.SetActive(true);
            zakljucano.SetActive(false);
        }

        PlayerPrefs.SetInt("SelectedDrone", currentDroneIndex_);
    }

    public void ChangePrevious()
    {
        DroneModels_[currentDroneIndex_].SetActive(false);

        currentDroneIndex_--;
        if (currentDroneIndex_ == -1)
            currentDroneIndex_ = DroneModels_.Length - 1;

        

        DroneBluePrintGaraza c = drones_[currentDroneIndex_];
        DroneModels_[currentDroneIndex_].SetActive(true);
        PlayBTN.gameObject.SetActive(false);
        zakljucano.SetActive(true);
        if (c.isUnlocked)
        {
            PlayBTN.gameObject.SetActive(true);
            zakljucano.SetActive(false);
        }

        PlayerPrefs.SetInt("SelectedDrone", currentDroneIndex_);
    }
}
