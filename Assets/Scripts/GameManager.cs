using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // poziva default background temu, brine se o svim zvukovima
    public string TrenutnaScena;
    void Start()
    {
        Scene scene = SceneManager.GetActiveScene();   // Vracamo trenutnu aktivnu scenu kako bidobili naziv trenutne scene
        TrenutnaScena = scene.name;
        if (TrenutnaScena == "Level6" || TrenutnaScena =="Level8")
        {
            AudioManager.Instance.Play(SoundType.Storm);
            AudioManager.Instance.Play(SoundType.Kisa);
        }
        if (TrenutnaScena == "Level2")
        {
            AudioManager.Instance.Play(SoundType.SumaStorm);
            AudioManager.Instance.Play(SoundType.Kisa);
        }
        else
        AudioManager.Instance.Play(SoundType.BackgroundTheme);

     
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

}
