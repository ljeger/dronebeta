using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform playerTransform;   //dohvacamo transform komponentu playera da postavimo kameru iza njega
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - playerTransform.position;   //na pocetku postavljamo udaljenost kamere od playera tako da oduzmemo
    }

    // Update is called once per frame Late update se poziva na kraju updatea
    void LateUpdate()
    {
        transform.position = playerTransform.position + offset; // u vsakoj update funkciji postavljamo kameru na poziciju playera + neki ofset
    }
}
