using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class StarsMenu : MonoBehaviour
{

   
    public static bool StarPanelActive = false;
    public GameObject StarPanelUI;
    private string TrenutnaScena;

    void Update()
    {
        if (StarPanelActive)
        {
            Time.timeScale = 0f;
        }
    }
        public void ResumeGoal()
    {
        StarPanelUI.SetActive(false);
        Time.timeScale = 1f; //vrati na normalu  //idi dalje do goal
        StarPanelActive = false;
    }

    public void RepeatGame()
    {
        SceneManager.LoadScene(TrenutnaScena);
    }

    void Start()
    {
        {
            Scene scene = SceneManager.GetActiveScene();   // Vracamo trenutnu aktivnu scenu kako bidobili naziv trenutne scene
            TrenutnaScena = scene.name;
        }

    }
}
