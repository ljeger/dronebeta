using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GarazaVSTrgovina : MonoBehaviour
{
    public void PlayTrgovina()
    {
        SceneManager.LoadScene("Trgovina");

    }
    public void PlayGaraza()
    {
        SceneManager.LoadScene("Garaza");

    }

    public void PlayBack()
    {
        SceneManager.LoadScene("Menu");
    }

    public void PlayLeveli()
    {
        SceneManager.LoadScene("Level_Menu");
    }

}
