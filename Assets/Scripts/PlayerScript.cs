using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
   
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bomba"))
        {
            AudioManager.Instance.Play(SoundType.Eksplozija);

           
        }

        if (collision.gameObject.CompareTag("Novcic"))
        { 
            AudioManager.Instance.Play(SoundType.Novcic);
            
        }

        if (collision.gameObject.CompareTag("Dijamant"))
            AudioManager.Instance.Play(SoundType.Dijamant);

        if (collision.gameObject.CompareTag("Krug"))
        {
            AudioManager.Instance.Play(SoundType.Krug);
            Debug.Log("Prosao si kroz krug");
        }

        if (collision.gameObject.CompareTag("Granica"))
            {

            GameObject.FindWithTag("Player").SetActive(false);
            PlayerManager.gameOver = true;
            Debug.Log("Ne mo�e� kratiti teren ili iza�i iz granica terena!!! ");
           
        }
    }


    void Start()
    {
       
     
        
    }

   

}

