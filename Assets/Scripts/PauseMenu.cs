using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;


public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public static bool pause;


    private void Start()
    {
        AudioListener.pause = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        } 
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f; //vrati na normalu
        GameIsPaused = false;
        AudioListener.pause = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;  //stopiraj igru
        GameIsPaused = true;
        AudioListener.pause = true;
    }

    public void LoadMenu()
    {
        //Time.timeScale = 1f; //nije vise na pauzi
        SceneManager.LoadScene("Menu");
      
    }

    public void QuiteGame()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}
