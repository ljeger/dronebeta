using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Money : MonoBehaviour
{
    public int CoinsSuma=0;
    public int currentCoins = 0;
    public int brojac = 0;
    [SerializeField]
    private Text coinCounter;
    void Start()
    {
        if (PlayerPrefs.HasKey("TNCoin"))
        {
            currentCoins = PlayerPrefs.GetInt("TNCoin");
            CoinsSuma = currentCoins;
        }
        else
        {
            CoinsSuma = 0;
            //coinCounter = currentCoins;
        }

    }

    // Update is called once per frame
    void Update()
    {
        coinCounter.text = "Coins: " + CoinsSuma.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Novcic"))

        {

            Debug.Log("Sudario si se s" + collision.gameObject.name);
            CoinsSuma++;
            brojac++;
            Destroy(collision.gameObject);
            //collision.gameObject.SetActive(false);
            Debug.Log("Coins " + CoinsSuma);
            Debug.Log("brojac" + brojac);


        }

    }  

    void OnTriggerEnter(Collider target)
    {
        if (target.CompareTag("Goal"))
        {
            Debug.Log("Spremanje novcica");
            currentCoins = PlayerPrefs.GetInt("TNCoin", 0); // ako nema nov�i�a onda �e vratiti 0  
            currentCoins += brojac;
            PlayerPrefs.SetInt("TNCoin", currentCoins);
            Debug.Log("brcoins" + CoinsSuma);
            Debug.Log("novcicisuma" + currentCoins);

        }
    }

   
}