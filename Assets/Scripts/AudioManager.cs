using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

    public AudioSound[] sounds;

    public AudioMixerGroup audioMixerGroup;

private AudioSound Find(SoundType type)
    {
        return Array.Find(sounds, sound => sound.type == type);
    }
    
    public void Play(SoundType type)
    {
        var playSound = Find(type);
        if (playSound != null)
        {
            playSound.source.Play();
        }
    }
//Razlika izmedu pause i play je kad pozovemo pauze klip se zaustavlja na neko mijesto i s play nastavljamo
    // Dok s stop se zaustavlja vraca pokazivac na pocetak i s play se playa ispocetka
   public void Pause(SoundType type)
    {
        var playSound = Find(type);
        if (playSound != null)
        {
            playSound.source.Pause();
        }
    }
    private void InitSources()
    {
        foreach (var sound in sounds)
        {
            var audioSource = gameObject.AddComponent<AudioSource>(); //AudioSound 
            audioSource.clip = sound.audioClip;


            audioSource.loop = sound.loop;
            audioSource.volume = sound.volume;
            audioSource.playOnAwake = false;
            sound.source = audioSource;
            audioSource.outputAudioMixerGroup = audioMixerGroup;

        }
    }

    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            return instance;
        }
    }
    void Awake()  //poziva se jednom prije funckije start kad objekt postaje vidljiv
    {
        if (instance == null)
        {
            instance = this;  //postavi staticnu referencu na obican gameobjekt
        }
        else
        {
            Destroy(gameObject);  //ako smo ga dodali na dvije scene unistimo drugi
            return;
        }
        DontDestroyOnLoad(gameObject);  //nece se unistit prilikom prelaska u drugu scenu vec se se prenjeti
        InitSources();
    }
}