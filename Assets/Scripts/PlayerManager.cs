using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static bool gameOver;
    public static bool finnished_3zvijezdice;
    public static bool finnished_2zvijezdice;
    public static bool finnished_1zvijezdice;
    public static bool finnished_0zvijezdice;
    public GameObject gameOverPanel;
    public GameObject StarPanel_0zvijezdice;
    public GameObject StarPanel_1zvijezdice;
    public GameObject StarPanel_2zvijezdice;
    public GameObject StarPanel_3zvijezdice;
    void Start()
    {
        gameOver = false;
        finnished_0zvijezdice = false;
        finnished_1zvijezdice = false;
        finnished_2zvijezdice = false;
        finnished_3zvijezdice = false;
        Time.timeScale = 1; 
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverPanel.SetActive(true);
        }

        else if (finnished_3zvijezdice)
        {
           // Time.timeScale = 0;
            StarPanel_3zvijezdice.SetActive(true);
        }

        else if (finnished_2zvijezdice)
        {
            //Time.timeScale = 0;
            StarPanel_2zvijezdice.SetActive(true);
        }

        else if (finnished_1zvijezdice)
        {
           // Time.timeScale = 0; 
            StarPanel_1zvijezdice.SetActive(true);
        }

        else if (finnished_0zvijezdice)
        {
           // Time.timeScale = 0;
            StarPanel_0zvijezdice.SetActive(true);
        }
    }
}
