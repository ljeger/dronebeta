using System.Collections;
using UnityEngine;

public class StopTimer : MonoBehaviour
{
    public GameObject Goal;
    void OnTriggerEnter(Collider target)
    {
        if (target.CompareTag("Player"))  //Trazis playera jer on na sebi ima nakacen timer
        {
            Goal.SetActive(true);  //stavi na active goal da ima gdje sletiti dron
            target.SendMessage("Finnish");   //saljes playeru poruku finnish da zaustavi timer i pali canvas starscanvas
            DestroyGameObject();   //unistava se gameobject stop timer
        }
       
    }
    void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}
