﻿using System;
using UnityEngine;
using Unity.Audio;

public enum SoundType
{
    Eksplozija,
    Novcic,
    BackgroundTheme,
    Dijamant,
    Krug,
    Storm,
    SumaStorm,
    Kisa
}
 
[System.Serializable]
public class AudioSound
{
    public SoundType type;
    public AudioClip audioClip;

    [HideInInspector]
    public AudioSource source;

    [Range(0f, 1f)]
    public float volume;

    //ne treba jer necemo loopat nista?
    public bool loop;

 


}
