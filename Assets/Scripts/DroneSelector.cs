using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneSelector : MonoBehaviour
{
    public int currentDroneIndex;
    public GameObject[] drones;

    void Start()
    {
        currentDroneIndex = PlayerPrefs.GetInt("SelectedDrone", 0);
        foreach(GameObject drone in drones)
            drone.SetActive(false);

        drones[currentDroneIndex].SetActive(true);
    }

}
