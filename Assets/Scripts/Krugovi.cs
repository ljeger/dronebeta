using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Krugovi : MonoBehaviour
{
    private int KrugoviSuma;
    private int Krug1Coins = 100;  //izos coinsa za 1 krug
    public int NovicicSuma;
    public string names;
    [SerializeField]
    private Text KrugoviCounter;
    void Start()
    {
        KrugoviSuma = 0;
       


    }

    // Update is called once per frame
    void Update()
    {
        KrugoviCounter.text = "Krugovi: " + KrugoviSuma.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Krug"))

        {
            names = gameObject.name;//provjeravamo koje je ime drona na pocetku
            Debug.Log(names);
            Debug.Log("Sudario si se s" + collision.gameObject.name);
            KrugoviSuma++;
            Destroy(collision.gameObject);
            Debug.Log("Krug " + KrugoviSuma);

        }
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.CompareTag("Goal"))  
        {
            if (KrugoviSuma == 1)  //ako na kraju kad dotaknes goal imas 1 krug
            {
                if (PlayerPrefs.HasKey("TNCoin"))   //ako postoji 
                {
                    NovicicSuma = PlayerPrefs.GetInt("TNCoin");
                    NovicicSuma = NovicicSuma + Krug1Coins;
                    PlayerPrefs.SetInt("TNCoin", NovicicSuma);
                }

                else
                {
                    NovicicSuma = 0;
                    NovicicSuma = NovicicSuma + Krug1Coins;
                    PlayerPrefs.SetInt("TNCoin", NovicicSuma);
                }


            }
          if (KrugoviSuma == 2)  //ako je skupio 2 kruga dobiva common dio tj. drugog drona iz skupa 
                //provjeravamo s kojim dronom igra tj koji mu je prvi dron u skupini
            {
                if(names == "_Drone [Toad] - Warhawk") //ako igra s prvim dronom iz 1 skupine otkljucavamo drugi
                {
                    PlayerPrefs.SetInt("_Drone [Toad] - Clownfish", 1);
                }
                if(names == "_Drone [BumbleBee] - Electric")
                {
                    PlayerPrefs.SetInt("_Drone [BumbleBee] - Golden", 1);
                }
                if(names== "_Drone [Quad] - Police")
                {
                   PlayerPrefs.SetInt("_Drone [Quad] - Space", 1);
                }
                if(names== "_Drone [UFO] - Alien")
                {
                    PlayerPrefs.SetInt("_Drone [UFO] - Ancient", 1);
                }
                }
            if (KrugoviSuma == 3)  //ako je skupio 3 kruga dobiva rare dio tj. treceg drona iz skupa 
                                   //provjeravamo s kojim dronom igra tj koji mu je prvi ili drugi dron u skupini
            {
                if (names == "_Drone [Toad] - Warhawk" || names== "_Drone[Toad] - Clownfish") //ako igra s prvim dronom iz 1 skupine otkljucavamo drugi
                {
                    PlayerPrefs.SetInt("_Drone [Toad] - Ladybug", 1);
                }
                if (names == "_Drone [BumbleBee] - Electric" || names == "_Drone [BumbleBee] - Golden")
                {
                    PlayerPrefs.SetInt("_Drone [BumbleBee] - Jungle", 1);
                }
                if (names == "_Drone [Quad] - Police" || names == "_Drone [Quad] - Space")
                {
                    PlayerPrefs.SetInt("_Drone [Quad] - Desert Recon", 1);
                }
                if (names == "_Drone [UFO] - Alien" || names == "_Drone [UFO] - Ancient")
                {
                    PlayerPrefs.SetInt("_Drone [UFO] - Crystal", 1);
                }

            }
            if (KrugoviSuma == 4)  //ako je skupio 4 kruga dobiva legendary dio tj. cetvrtog drona iz skupa 
                                   //provjeravamo s kojim dronom igra tj koji mu je prvi,drugi ili treci dron u skupini
            {
                if (names == "_Drone [Toad] - Warhawk" || names == "_Drone[Toad] - Clownfish" || names == "_Drone[Toad] - Ladybug") //ako igra s prvim dronom iz 1 skupine otkljucavamo drugi
                {
                    PlayerPrefs.SetInt("_Drone [Toad] - Racecar", 1);
                }
                if (names == "_Drone [BumbleBee] - Electric" || names == "_Drone [BumbleBee] - Golden" || names== "_Drone[BumbleBee] - Jungle")
                {
                    PlayerPrefs.SetInt("_Drone [BumbleBee] - Silver", 1);
                }
                if (names == "_Drone [Quad] - Police" || names == "_Drone [Quad] - Space" || names == "_Drone [Quad] - Desert Recon")
                {
                    PlayerPrefs.SetInt("_Drone [Quad] - Disco", 1);
                }
                if (names == "_Drone [UFO] - Alien" || names == "_Drone [UFO] - Ancient" || names == "_Drone [UFO] - Crystal")
                {
                    PlayerPrefs.SetInt("_Drone [UFO] - Marine", 1);
                }
            }
            }

    }
}

